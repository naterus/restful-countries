package com.rca.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rca.api.dao.AppCountry;
import com.rca.api.dao.AppLink;
import com.rca.api.dto.CountryData;
import com.rca.api.entities.Country;
import com.rca.api.repositories.CountryRepository;

@Service
public class CountryService {
  @Autowired
  CountryRepository ctrRepo;
  
  @Autowired 
  StateService stService;
 
  @Autowired 
  NavigationServiceLocal navService;
  
  public List<AppCountry> getAllCountries(){
	  List<Country> countries = ctrRepo.findAll();
	  List<AppCountry> appCountries  = getAppCountrySlim(countries);

	  return  appCountries;
  }
  
  public List<AppCountry> getAppCountrySlim(List<Country> countries) {
	  List<AppCountry> appCountries = new ArrayList<>();
	  
		for(Country county : countries) {
			if(county!= null) {
				AppCountry country = getAppCountry(county);
				appCountries.add(country);
			}
		}
	  
	  return appCountries;
  }
  
  public AppCountry getAppCountry(Country country) {
	  AppCountry appCountry = new AppCountry();
	  List<AppLink> links = navService.getCountryLink(country.getName());
	  
	  appCountry.setName(country.getName());
	  appCountry.setShortName(country.getShortName());
	  appCountry.setCode(country.getCode());
	  appCountry.setCapital(country.getCapital());
	  appCountry.setIso(country.getIso());
	  appCountry.setCurrency(country.getCurrency());
	  appCountry.setDescription(country.getDescription());
	  appCountry.setFlag("localhost:8080/images/flags/"+country.getName().replaceAll(" ","-")+".png");
	  appCountry.setPopulation(country.getPopulation());
	  appCountry.setPresident(country.getPresident());
	  appCountry.setLinks(links);
	  appCountry.setStates(stService.getAppStates(country.getStates()));
	  
	  return appCountry;
	  
  }
  
  
  
  public AppCountry getCountry(String name){
	  Country country =  ctrRepo.findByNameIgnoreCase(name);
	  AppCountry appCountry = getAppCountry(country);
	  return appCountry;
  }
  
  
  public Optional<CountryData> getCountryByCode(String code){
	  
	  return ctrRepo.findByCode(code);
  }
  
  
  
  
//  public Country getCountry(Object nameOrId){
//	  return ctrRepo.findCountryByIdOrName(nameOrId);
//  }
  
  

}
