package com.rca.api.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rca.api.dao.AppDistrict;
import com.rca.api.dao.AppLink;
import com.rca.api.entities.District;
import com.rca.api.repositories.DistrictRepository;

@Service
public class DistrictService {
	
	@Autowired
	DistrictRepository distRepo;
	
	@Autowired
	NavigationServiceLocal navService;
	
	public List<AppDistrict> getDistricts(Long state) {
		List<District>  districts =  distRepo.getDistrictByState(state); 
		List<AppDistrict> appDistricts = getAppDistricts(districts);
		
		return appDistricts;
	}
	
	public List<AppDistrict> getAppDistricts(List<District> districts){
		List<AppDistrict> appDistricts = new ArrayList<>();
		
		for(District district : districts) {
			appDistricts.add(getAppDistrict(district));
		}
		
		return appDistricts;
	}
	
	public AppDistrict getAppDistrict(District district) {
		AppDistrict appDistrict = new AppDistrict();
		
		appDistrict.setDistrictName(district.getDistrictName());
		appDistrict.setDescription(district.getDescription());
		appDistrict.setPopulation(district.getPopulation());
		appDistrict.setState(district.getState().getName());
		appDistrict.setLinks(navService.getDistrictLink(district.getState().getCountry().getName(), district.getState().getName(), district.getDistrictName()));
		return appDistrict;
	}

}
