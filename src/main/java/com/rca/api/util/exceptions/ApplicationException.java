package com.rca.api.util.exceptions;

public class ApplicationException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Integer status;
	int code;
	String message;
	String _link;
	
	
   
	
	
	public ApplicationException(Integer status, int code, String message, String _link) {
		super();
		this.status = status;
		this.code = code;
		this.message = message;
		this._link = _link;
	}
	
	public ApplicationException() {
		
	}
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String get_link() {
		return _link;
	}
	public void set_link(String _link) {
		this._link = _link;
	}
	
	
	
}
