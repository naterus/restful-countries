package com.rca.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.rca.api.dto.CountryData;
import com.rca.api.entities.Country;

public interface CountryRepository extends CrudRepository<Country, CountryData>{

	@Override
	List<Country> findAll();

    Country findById(Long id);
    
    Country findByNameIgnoreCase(String name);
    
    Optional<CountryData> findByCode(String code);
    
  
	
	   //Query using Id or country name
//	  @Query(nativeQuery=true, value="SELECT country_id, country_name,description,flag,president,nick_name, short_name , population FROM countries WHERE (country_name = :nameOrId)")
//	  Country findCountryByIdOrName(@Param("nameOrId") Object nameOrId);
	  
//	  @Query(nativeQuery=true, value="SELECT country_id, country_name,description,flag,president,nick_name, short_name , population FROM countries WHERE country_id = :id ")
//	  Country getCountryById(@Param("id")int id);
}
