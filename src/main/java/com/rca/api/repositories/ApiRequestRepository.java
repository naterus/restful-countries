package com.rca.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.rca.api.entities.ApiRequest;


public interface ApiRequestRepository extends CrudRepository<ApiRequest,Long>{
   
	@Query(nativeQuery = true, value = "SELECT * FROM requests WHERE (api_key = :user)")
	public List<ApiRequest> getByUser(@Param("user") String apiKey);
	
	
	@Query(nativeQuery=true, value="SELECT  COUNT(*) FROM requests WHERE (status = :status) AND (api_key = :user)")
	public Long getByStatus(@Param("status") Boolean status,@Param("user") String apiKey);
	
	
	@Query(nativeQuery = true, value = "SELECT COUNT(*) FROM requests WHERE (api_key = :user)")
	public Long getByUserCount(@Param("user") String apiKey);
	
}
