package com.rca.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rca.api.entities.User;
import com.rca.api.repositories.UserRepository;

@Service
public class UserService {
    
	@Autowired
	UserRepository userRepo;
	
	public Long getAllUsersNum() {
		Long users = userRepo.getUsersTotal();
		return users;
	}
	
	
	public void saveNewUser(User user) {
		userRepo.save(user);
		
	}
	
	
	public User loadUserByUsername(String email) throws UsernameNotFoundException {
	    User user = userRepo.findByEmail(email);

	    if (user == null) {
	        throw new UsernameNotFoundException("Not found!");
	    }

	    return user;
	}

	
}
