package com.rca.api.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "states")
public class State {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "state_seq")
	@SequenceGenerator(name = "state_seq",sequenceName = "state_seq",  allocationSize = 1)
	@Column(nullable = false)
	private long id;
	
	private String name;
	
	@Column(nullable = true)
	private Long population;

	
	@Column(nullable = true)
	private String governor;
	
	@Column(nullable = true, length = 600)
	private String description;
	
	@Column(nullable = true)
	private Date createdAt;
	
	
	@Column(nullable = true)
	private Date updatedAt;
	
	@OneToMany(mappedBy =  "state") 
	private List<District> districts;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.PERSIST },
			fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	@JsonIgnore
	private Country country;
	
	
	public State() {
		
	}

	

	public State(String name) {
		super();
		this.name = name;

	}


	

	public Country getCountry() {
		return country;
	}



	public void setCountry(Country country) {
		this.country = country;
	}



	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}


	public String getGovernor() {
		return governor;
	}

	public void setGovernor(String governor) {
		this.governor = governor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

	public Date getCreatedAt() {
		return createdAt;
	}



	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}



	public Date getUpdatedAt() {
		return updatedAt;
	}



	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



	public List<District> getDistricts() {
		return districts;
	}

	public void setDistricts(List<District> districts) {
		this.districts = districts;
	}

	
	

}
