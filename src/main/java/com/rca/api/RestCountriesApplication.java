package com.rca.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.rca.api.repositories.CountryRepository;
import com.rca.api.repositories.DistrictRepository;
import com.rca.api.repositories.StateRepository;

@SpringBootApplication
public class RestCountriesApplication {
	
	@Autowired
	CountryRepository countRepo;
	@Autowired
	StateRepository stateRepo;
	@Autowired
	DistrictRepository distRepo;

	public static void main(String[] args) {
		SpringApplication.run(RestCountriesApplication.class, args);
	}
	
	
//	@Bean
//	CommandLineRunner runner() {
//		return args -> {
//			Country ctr1 = new Country("Nigeria", "Ng");
//			Country ctr2 = new Country("China", "cn");
//			Country ctr3 = new Country("America", "Am");
//
//			State st1 = new State("Abuja");
//			State st2 = new State("Kaduna");
//
//			District dist1 = new District("Kaura");
//			District dist2 = new District("Chikun");
//			District dist3 = new District("	Wuse");
//			
//			//Manually set relationships
//			st1.setCountry(ctr1);
//			st2.setCountry(ctr1);
//			
//			//Manually set relationships
//			dist1.setState(st2);
//			dist2.setState(st2);
//			dist3.setState(st1);
//	
//			
//						
//						// save Countries in database
//
//						countRepo.save(ctr1);
//						countRepo.save(ctr2);
//						countRepo.save(ctr3);
//						
//						
//						// save States in database
//
//						stateRepo.save(st1);
//						stateRepo.save(st2);
//						
//						// save Districts in database
//						distRepo.save( dist1);
//						distRepo.save( dist2);
//						distRepo.save( dist3);
//		};
//	}


}
