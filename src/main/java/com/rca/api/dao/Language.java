package com.rca.api.dao;

public class Language {

	private String name;
	private String locality;
	
	public Language() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	
	
	
}
