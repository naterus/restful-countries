package com.rca.api.dto;

import java.util.List;

import com.rca.api.dao.AppLink;
import com.rca.api.entities.State;

public interface CountryData {

	public Long getId();
	public String getName();
	public String getShortName();
	public String getDescription();
	public String getPopulation();
	public String getNickName();
	public String getPresident();
	public List<AppLink> getLinks();
	public List<State> getStates();
}
