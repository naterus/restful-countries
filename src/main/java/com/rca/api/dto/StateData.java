package com.rca.api.dto;

import java.util.List;

import com.rca.api.entities.District;

public interface StateData {
    public String getName();
    public String getPopulation();
    public List<District> getDistricts();
}
