package com.rca.api.dao;

import java.util.List;

public class AppDistrict {
	
	private String districtName;
	
	private Long population;
	
	private String state;
	
	private String description;
	
	private List<AppLink> links;

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<AppLink> getLinks() {
		return links;
	}

	public void setLinks(List<AppLink> links) {
		this.links = links;
	}
	
	
	
	
	
}
