package com.rca.api.services;

import java.util.List;

import org.springframework.context.annotation.Bean;

import com.rca.api.dao.AppLink;

public interface NavigationServiceLocal {

	List<AppLink> getAppLink();
	
	List<AppLink> getCountryLink(String countryId);
	
	List<AppLink> getStateLink(String countryId,String stateId);
	
	List<AppLink> getDistrictLink(String countryId,String stateId, String districtId);
}
