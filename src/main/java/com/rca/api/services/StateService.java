package com.rca.api.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rca.api.dao.AppLink;
import com.rca.api.dao.AppState;
import com.rca.api.entities.Country;
import com.rca.api.entities.State;
import com.rca.api.repositories.CountryRepository;
import com.rca.api.repositories.StateRepository;

@Service
public class StateService {
    
	  @Autowired
	  StateRepository stRepo;
	  
	  @Autowired
	  NavigationServiceLocal navService;
	  
	  @Autowired
	  DistrictService dtService;
	  
	  @Autowired 
	  CountryRepository ctrRepo;
	  
	  public List<AppState> getStates(Long id){
		  
		  List<State> states = stRepo.findByCountryIgnoreCase(id);
		  List<AppState> appStates = getAppStates(states);
		  
		  return appStates;
	  }
	  
	  public List<AppState> getAppStates(List<State> states){
		  List<AppState> appStates = new ArrayList<>();
		  
		  for(State state : states) {
			  if(state != null) {
			       appStates.add(getState(state));
			  }
		  }
		  
		  return appStates;
		  
	  }
	  
	  public AppState getState(State state) {
		  AppState appState = new AppState();
		  String countryId = state.getCountry().getName();
		  List<AppLink> links = navService.getStateLink(countryId,state.getName());
		  
		  appState.setName(state.getName());
		  appState.setCountry(state.getCountry().getName());
		  appState.setDescription(state.getDescription());
		  appState.setPopulation(state.getPopulation());
		  appState.setGovernor(state.getGovernor());
		  appState.setLinks(links);
		  appState.setDistricts(dtService.getAppDistricts(state.getDistricts()));
		  
		  return appState;
		  
	  }
	  
	  
	  public AppState getStateByName(Long id,String stateName) {
		  State state = stRepo.findByNameIgnoreCase(id,stateName);
		  AppState appState =getState(state);
		  return appState;
	  }
}
