package com.rca.api.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "requests")
public class ApiRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "request_seq")
	@SequenceGenerator(name = "request_seq", sequenceName = "request_seq", allocationSize = 1)
	private Long id;
	
	@Column(nullable = true)
	private String endPoint;
	
	@Column(unique = false)
	private String apiKey;
	
	@Column(nullable = false)
	private Boolean status;
	
	private Date createdAt = new Date();
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.PERSIST },
			fetch = FetchType.LAZY)
	@JoinColumn(name= "user")
	private User user;
	
	public ApiRequest() {
		
	}
	
	


	public ApiRequest(String endPoint, String apiKey, Boolean status, Date createdAt, User user) {
		super();
		this.endPoint = endPoint;
		this.apiKey = apiKey;
		this.status = status;
		this.createdAt = createdAt;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	

	public Boolean getStatus() {
		return status;
	}



	public void setStatus(Boolean status) {
		this.status = status;
	}



	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}


}
