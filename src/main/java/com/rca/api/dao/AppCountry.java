package com.rca.api.dao;

import java.util.Date;
import java.util.List;

public class AppCountry {
	
	private  String name;
	
	private String shortName;
	
	private String nickName;
	
	private String code;
	
	private String capital;
	
	private String iso;
	
	private Long population;
	
	private String currency;
	
	private String race;
	
	private String continent;
	
	private Date nationalHoliday;
	
	private Date independenceDate;
	
	private String religion;
	
	private List<Language> languages;
	
	private String officialLanguage;
	
	private String flag;
	
	private String description;
	
	private String president;
	
	private List<AppLink> links;
	
	private List<AppState> states;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRace() {
		return race;
	}
	
	

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public Date getNationalHoliday() {
		return nationalHoliday;
	}

	public void setNationalHoliday(Date nationalHoliday) {
		this.nationalHoliday = nationalHoliday;
	}

	public Date getIndependenceDate() {
		return independenceDate;
	}

	public void setIndependenceDate(Date independenceDate) {
		this.independenceDate = independenceDate;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	public String getOfficialLanguage() {
		return officialLanguage;
	}

	public void setOfficialLanguage(String officialLanguage) {
		this.officialLanguage = officialLanguage;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPresident() {
		return president;
	}

	public void setPresident(String president) {
		this.president = president;
	}

	public List<AppLink> getLinks() {
		return links;
	}

	public void setLinks(List<AppLink> links) {
		this.links = links;
	}

	public List<AppState> getStates() {
		return states;
	}

	public void setStates(List<AppState> states) {
		this.states = states;
	}
	
	
	
}
