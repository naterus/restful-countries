package com.rca.api.dao;

import java.util.List;


import com.rca.api.entities.District;

public class AppState {
	
    
    private String name;
    
    private String country;
	
	private Long population;

	private String governor;
	
	private String description;
	
	private List<AppDistrict> districts;
	
	private List<AppLink> links;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getGovernor() {
		return governor;
	}

	public void setGovernor(String governor) {
		this.governor = governor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<AppDistrict> getDistricts() {
		return districts;
	}

	public void setDistricts(List<AppDistrict> districts) {
		this.districts = districts;
	}

	public List<AppLink> getLinks() {
		return links;
	}

	public void setLinks(List<AppLink> links) {
		this.links = links;
	}
	
	
	
	

}
