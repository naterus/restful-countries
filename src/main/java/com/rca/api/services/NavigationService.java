package com.rca.api.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.rca.api.dao.AppCountry;
import com.rca.api.dao.AppLink;

@Service
public class NavigationService implements NavigationServiceLocal{

	//API Version 1
	public List<AppLink> getAppLink() {
		List<AppLink> links = new ArrayList<AppLink>();
		
		return links;
	}
	
	public List<AppLink> getCountryLink(String countryId){
		List<AppLink> links = new ArrayList<AppLink>();
		
		//Add self link
		AppLink selfLink = new AppLink();
		String selfHref ="api/v1/countries/"+countryId;
		selfLink.setName("self");
		selfLink.setHref(selfHref);
		selfLink.setRel("country");
		selfLink.setType("get");
		
		links.add(selfLink);
		
		return links;
	}
	
	public List<AppLink> getStateLink(String countryId,String stateId){
		List<AppLink> links = new ArrayList<>();
		
		//Add self link
		AppLink selfLink = new AppLink();
		String selfHref ="api/v1/countries/"+countryId+"/states/"+stateId;
		selfLink.setName("self");
		selfLink.setHref(selfHref);
		selfLink.setRel("state");
		selfLink.setType("get");
		
		links.add(selfLink);
		return links;
	}
	
	public List<AppLink> getDistrictLink(String countryId,String stateId,String districtId){
		List<AppLink> links = new ArrayList<>();
		
		//Add self link
		AppLink selfLink = new AppLink();
		String selfHref ="api/v1/countries/"+countryId+"/states/"+stateId+"/districts/"+districtId;
		selfLink.setName("self");
		selfLink.setHref(selfHref);
		selfLink.setRel("districts");
		selfLink.setType("get");
				
		links.add(selfLink);
		
		return links;
		
	}
	
	//API Version 2
	
}
