package com.rca.api.controllers;

import java.security.SecureRandom;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rca.api.entities.ApiRequest;
import com.rca.api.entities.User;
import com.rca.api.services.ApiRequestService;
import com.rca.api.services.UserService;


@Controller
@RequestMapping("/developer")
public class UsersController {
	
	@Autowired
	UserService userServ;
	
	@Autowired
	ApiRequestService reqServ;
	
	
	//SecureRandom string generator 
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();

	String randomString( int length ){
	   StringBuilder sb = new StringBuilder( length );
	   for( int i = 0; i < length; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	//End SecureRandom string generator

	@GetMapping("dashboard")
	public String dashboard(Authentication auth,Model model) {
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		Long successRequests = reqServ.getSuccessRequests(user.getApiKey());
		Long failedRequests = reqServ.getFailedRequests(user.getApiKey());
		Long allRequests = reqServ.getAllRequests(user.getApiKey());
		model.addAttribute("user",user);
		model.addAttribute("successRequests",successRequests);
		model.addAttribute("failedRequests",failedRequests);
		model.addAttribute("allRequests",allRequests);
		return "users/dashboard";
	}
	
	@GetMapping("api-key")
	public String apiKey(Authentication auth,Model model) {
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		model.addAttribute("user",user);
		return "users/api-key";
	}
	
	@PostMapping("api-key/generate")
	public String generateApiKey(RedirectAttributes attr,Authentication auth,Model model) {
		
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		String apiKey = "RCA"+randomString(30);
		String success = "Api key generated successfully.";
		attr.addFlashAttribute("success",success);
		user.setApiKey(apiKey);
		userServ.saveNewUser(user);
		return "redirect:/developer/api-key";
	}
	
	@PostMapping("api-key/reset")
	public String resetApiKey(RedirectAttributes attr,Authentication auth,Model model) {
		
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		String apiKey = "RCA"+randomString(30);
		String success = "Api key reset successful.";
		attr.addFlashAttribute("success",success);
		user.setApiKey(apiKey);
		userServ.saveNewUser(user);
		return "redirect:/developer/api-key";
	}
	
	@GetMapping("profile")
	public String profile(Authentication auth,Model model) {
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		model.addAttribute("user",user);
		return "users/profile";
	}
	
	@PostMapping("profile/update")
	public String updateProfile(Authentication auth,User user,RedirectAttributes attr, Model model) {
		String email = auth.getName();
		User oldUser = userServ.loadUserByUsername(email);
		user.setPassword(oldUser.getPassword());
		user.setRole(oldUser.getRole());
		user.setStatus(oldUser.getStatus());
		user.setCreatedAt(oldUser.getCreatedAt());
		user.setApiKey(oldUser.getApiKey());
		userServ.saveNewUser(user);
		model.addAttribute("user",user);
		String success = "Profile updated successfully.";
		attr.addFlashAttribute("success",success);
		return "redirect:/developer/profile";
	}
	
	@GetMapping("api-requests")
	public String requests(Authentication auth,Model model) {
		String email = auth.getName();
		User user = userServ.loadUserByUsername(email);
		List<ApiRequest> requests = reqServ.getUserRequests(user.getApiKey());
		
		model.addAttribute("user",user);
		model.addAttribute("requests",requests);
		return "users/requests";
	}
	
	@GetMapping("logout")
	public String logout(HttpServletRequest request,HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null) {
			new SecurityContextLogoutHandler().logout(request,response,auth);
		}
		
		return "redirect:/login?logout";
		
		
	}

}
