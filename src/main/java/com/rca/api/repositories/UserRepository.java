package com.rca.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rca.api.entities.User;

public interface UserRepository extends CrudRepository<User,Long>{
    
	//@Query(nativeQuery=true, value="SELECT name,email,api_key,id,password,created_at,status,role FROM user_accounts WHERE (email = email)")
	User findByEmail(String email);
	
	@Override
	List<User> findAll();
	
	User findByApiKey(String apiKey);
	
	@Query(nativeQuery=true, value="SELECT COUNT(*) FROM user_accounts")
	Long getUsersTotal();
	
}
