package com.rca.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rca.api.entities.User;
import com.rca.api.services.UserService;

@Controller
@RequestMapping("/")
//@PreAuthorize("permitAll()")
public class HomeController {
	
	@Autowired
	UserService userServ;
	
	@Autowired
	BCryptPasswordEncoder bCryptEncoder;

	
	@GetMapping
	public String index() {
		
		return "index";
	}
	
	@GetMapping("docs")
	public String doc() {
		
		return "redirect:docs/1.0";
		
	}
	
	@GetMapping("docs/{version}")
	public String docs(@PathVariable double version) {
		if(version == 1.0){
			return "docs/1.0";
		}
		
		return "errors/404";
		
	}
	
	@GetMapping("login")
	public String login(Model model) {
		User user = new User();
		model.addAttribute(user);
		return "login";
	}
	
//	@PostMapping("login/auth")
//	public String loginAuth(User user,Model model,RedirectAttributes attr) {
//		
//		if(user.getEmail() == "nathandauda27@gmail.com") {
//          String success = "Login successful";
//          attr.addFlashAttribute("success",success);
//          attr.addFlashAttribute("username",user.getEmail());
//          return "redirect:/login";
//		}
//		
//		String error = "Login failed";
//		attr.addFlashAttribute("error",error);
//		attr.addFlashAttribute("username",user.getEmail());
//	    return "redirect:/login";
//	
//	}
	
	@GetMapping("register")
	public String register(Model model) {
		User user = new User();
		model.addAttribute(user);
		return "register";
	}
	
	@PostMapping("register/save-user")
	public String saveUser(User user ,Model model,RedirectAttributes attr) {
		user.setPassword(bCryptEncoder.encode(user.getPassword()));
		userServ.saveNewUser(user);
		String success = "Account created successfully.";
		attr.addFlashAttribute("success",success);
		return "redirect:/login";
	}
	
	@GetMapping("pasword-reset")
	public String passwordReset() {
		return "password-reset";
	}
	
}
