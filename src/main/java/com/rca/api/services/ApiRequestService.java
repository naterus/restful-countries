package com.rca.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rca.api.entities.ApiRequest;
import com.rca.api.repositories.ApiRequestRepository;

@Service
public class ApiRequestService {
	
	@Autowired
	ApiRequestRepository reqRepo;

	public List<ApiRequest> getUserRequests(String apiKey){
		
		return reqRepo.getByUser(apiKey);
		
	}
	
	public Long getSuccessRequests(String apiKey) {
		return reqRepo.getByStatus(true,apiKey);
	}
	
	public Long getFailedRequests(String apiKey) {
		return reqRepo.getByStatus(false,apiKey);
	}
	
	public Long getAllRequests(String apiKey) {
		return reqRepo.getByUserCount(apiKey);
	}
	
	
	
}
