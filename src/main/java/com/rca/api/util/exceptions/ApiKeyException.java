package com.rca.api.util.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Api key invalid, kindly logon to our developer portal https://www.restfulcountries/login  and generate an Api Key.")
public class ApiKeyException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
