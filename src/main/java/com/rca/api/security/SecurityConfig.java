package com.rca.api.security;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	
	@Autowired
	BCryptPasswordEncoder bCryptEncoder;
	
	@Autowired
	DataSource dataSource;
	
	//JDBC Authentication
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.jdbcAuthentication().dataSource(dataSource)
	           .usersByUsernameQuery("SELECT email,password, status"+
			          " FROM user_accounts WHERE email = ?"
	        		   )
	           .authoritiesByUsernameQuery("SELECT email, role FROM "+
	        		   "user_accounts WHERE email = ?"
	        		   )
	           .passwordEncoder(bCryptEncoder);
		}
		
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		
		http.csrf().ignoringAntMatchers("/h2-console/**").and()
		
		//When api has a post endpoint add this to ignore
		//.csrf().ignoringAntMatchers("api/**").and() 
		.authorizeRequests()
		      .antMatchers("/developer/**").authenticated()
		      .and()
		         .formLogin().loginPage("/login")
		         .defaultSuccessUrl("/developer/dashboard", true)
		         .usernameParameter("email")
//		      .and()
//		         .oauth2Login()
//		         .loginProcessingUrl("/login/oauth/google")
//		         .and().csrf().disable()
		         ;
		
		
	    http.headers().frameOptions().disable();
		
	}
	
	
}
