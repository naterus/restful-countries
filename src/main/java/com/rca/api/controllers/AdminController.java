package com.rca.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rca.api.entities.User;
import com.rca.api.services.UserService;

@Controller
@RequestMapping("/section/admin")
public class AdminController {
	
	@Autowired 
	UserService userServ;
   
	@GetMapping("login")
	public String login(Model model,@RequestParam String token) {
		User user = new User();
		model.addAttribute(user);
		return "admin/login";
	}
	
	@GetMapping("dashboard")
	public String dashboard(Model model) {
		Long users = userServ.getAllUsersNum();
		model.addAttribute("users",users);
		return "admin/dashboard";
	}
	
	@GetMapping("countries")
	public String countries() {
		return "admin/countries";
	}
	
	@GetMapping("states")
	public String states(@PathVariable String countryId) {
		return "admin/countries";
	}
}
