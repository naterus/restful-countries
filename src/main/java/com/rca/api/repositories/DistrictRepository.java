package com.rca.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.rca.api.entities.District;

public interface DistrictRepository extends CrudRepository<District, String>{
   
	
	@Query(nativeQuery = true, value="SELECT * FROM districts WHERE (state = :id)")
	List<District> getDistrictByState(@Param("id") Long state);
	
	@Query(nativeQuery = true, value="SELECT * FROM districts WHERE (state = :stateId) AND (district_name = :districtName)")
	District getDistrict(@Param("stateId") Long stateId , @Param("districtName") String districtName);
}
