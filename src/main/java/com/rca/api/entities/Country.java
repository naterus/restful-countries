package com.rca.api.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "countries")
public class Country {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(nullable = false)
	private Long id;
	
	private  String name;
	
	@Column(nullable = true)
	private String shortName;
	
	@Column(nullable = true)
	private String nickName;
	
	@Column(nullable = true)
	private String code;
	
	@Column(nullable = true)
	private Long population;
	
	@Column(nullable = true)
	private String flag;
	
	@Column(nullable = true)
	private String currency;
	
	@Column(nullable = true)
	private String iso;
	
	@Column(nullable = true)
	private String capital;
	
	@Column(nullable = true, length = 600)
	private String description;
	
	@Column(nullable = true)
	private String president;
	
	@Column(nullable = true)
	private String officialLanguage;
	
	@Column(nullable = true)
	private String languages;
	
	@Column(nullable = true)
	private String continent;
	
	@Column(nullable = true)
	private String size;
	
	@Column(nullable = true)
	private String region;
	
	@Column(nullable = true)
	private Date nationalHoliday;
	
	@Column(nullable = true)
	private String race;
	
	@Column(nullable = true)
	private Date independence;
	
	@Column(nullable = true)
	private Date createdAt;
	
	
	@Column(nullable = true)
	private Date updatedAt;

    
	@OneToMany(mappedBy = "country") 
	private List<State> states;
	
	public Country() {
		
	}
	

	public Country(String name, String shortName, String nickName, String code, Long population, String flag,
			String currency, String iso, String capital, String description, String president, String officialLanguage,
			String languages, String continent, String size, String region, Date nationalHoliday, String race,
			Date independence, Date createdAt, Date updatedAt, List<State> states) {
		super();
		this.name = name;
		this.shortName = shortName;
		this.nickName = nickName;
		this.code = code;
		this.population = population;
		this.flag = flag;
		this.currency = currency;
		this.iso = iso;
		this.capital = capital;
		this.description = description;
		this.president = president;
		this.officialLanguage = officialLanguage;
		this.languages = languages;
		this.continent = continent;
		this.size = size;
		this.region = region;
		this.nationalHoliday = nationalHoliday;
		this.race = race;
		this.independence = independence;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.states = states;
	}








	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getNickName() {
		return nickName;
	}
	
	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPresident() {
		return president;
	}

	public void setPresident(String president) {
		this.president = president;
	}
	
	

	public String getCurrency() {
		return currency;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}



	public String getIso() {
		return iso;
	}



	public void setIso(String iso) {
		this.iso = iso;
	}



	public String getCapital() {
		return capital;
	}



	public void setCapital(String capital) {
		this.capital = capital;
	}
	
	



	public String getOfficialLanguage() {
		return officialLanguage;
	}



	public void setOfficialLanguage(String officialLanguage) {
		this.officialLanguage = officialLanguage;
	}



	public String getLanguages() {
		return languages;
	}



	public void setLanguages(String languages) {
		this.languages = languages;
	}



	public String getContinent() {
		return continent;
	}



	public void setContinent(String continent) {
		this.continent = continent;
	}



	public String getSize() {
		return size;
	}



	public void setSize(String size) {
		this.size = size;
	}



	public String getRegion() {
		return region;
	}



	public void setRegion(String region) {
		this.region = region;
	}



	public Date getNationalHoliday() {
		return nationalHoliday;
	}



	public void setNationalHoliday(Date nationalHoliday) {
		this.nationalHoliday = nationalHoliday;
	}



	public String getRace() {
		return race;
	}



	public void setRace(String race) {
		this.race = race;
	}



	public Date getIndependence() {
		return independence;
	}



	public void setIndependence(Date independence) {
		this.independence = independence;
	}



	public Date getCreatedAt() {
		return createdAt;
	}



	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}



	public Date getUpdatedAt() {
		return updatedAt;
	}



	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}
	
	
	
	
}
