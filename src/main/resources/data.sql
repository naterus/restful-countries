-- INSERT COUNTRIES			
insert into countries (id, name , short_name) values (nextval('country_seq'), 'Nigeria', 'Ngn');
insert into countries (id, name , short_name) values (nextval('country_seq'), 'China', 'Cn');
insert into countries (id, name , short_name) values (nextval('country_seq'), 'South Africa', 'Sa');

-- INSERT STATES			
insert into states (id,country, name) values (nextval('state_seq'),'1','Kaduna');
insert into states (id,country, name) values (nextval('state_seq'),'2','Sun City');
insert into states (id,country, name) values (nextval('state_seq'),'3','Hong');
insert into states (id,country, name) values (nextval('state_seq'),'1','Abuja');

-- INSERT DISTRICTS
insert into districts (district_id,district_name,state) values (nextval('district_seq'),'Kajuru','1');
insert into districts (district_id,district_name,state) values (nextval('district_seq'),'Kiaro','2');
insert into districts (district_id,district_name,state) values (nextval('district_seq'),'Kong','3');
insert into districts (district_id,district_name,state) values (nextval('district_seq'),'Wuse','4');
insert into districts (district_id,district_name,state) values (nextval('district_seq'),'Wuye','4');