package com.rca.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.rca.api.entities.State;

public interface StateRepository extends CrudRepository<State , String>{
	

	//Query using Id or country name
	  @Query(nativeQuery=true, value="SELECT * FROM states WHERE (country = :id)")
	  List<State> findByCountryIgnoreCase(@Param("id") Long id);
	  
	  @Query(nativeQuery=true, value="SELECT * FROM states WHERE (country = :id) AND (name = :name)")
	 // @Query(nativeQuery=true, value="SELECT state_id, state_name,country,description,governor, population FROM states WHERE (name = :name)")
	  State findByNameIgnoreCase(@Param("id") Long country, @Param("name") String stateName);
	  

}
