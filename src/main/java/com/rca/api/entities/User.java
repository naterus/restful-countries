package com.rca.api.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="user_accounts")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_seq")
	@SequenceGenerator(name="user_seq", sequenceName = "user_seq" , allocationSize  = 1)
	private Long id;
	
	@Column(length = 100)
	private String name;
	
	@Column(nullable = false, unique = true)

	private String email;
	
	@Column(nullable = true)
	public String userCountry;
	
	@Column(length = 100)
	private String password;
	
	@Column(length = 50 , unique = true , nullable = true)
	private String apiKey;
	
	private boolean status = true;
	
	@Column(length = 50)
	private String role = "User";
	
	private Date createdAt = new Date();
	
	@OneToMany(mappedBy = "user")	
	private List <ApiRequest> requests;
	
	public User() {
		
	}
	
	public User(String name, String email, String userCountry, String password, String apiKey, boolean status,
			String role, Date createdAt, List<ApiRequest> requests) {
		super();
		this.name = name;
		this.email = email;
		this.userCountry = userCountry;
		this.password = password;
		this.apiKey = apiKey;
		this.status = status;
		this.role = role;
		this.createdAt = createdAt;
		this.requests = requests;
	}



	public String getApiKey() {
		return apiKey;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getStatus() {
		return status;
	}
	
	

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}




	public List<ApiRequest> getRequests() {
		return requests;
	}




	public void setRequests(List<ApiRequest> requests) {
		this.requests = requests;
	}
	
	
	

}
