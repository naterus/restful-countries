package com.rca.api.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "districts")
public class District {
    
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "district_seq")
	@SequenceGenerator(name = "district_seq",sequenceName = "district_seq",  allocationSize = 1)
	private long districtId;
	
	@Column(nullable = false)
	private String districtName;
	
	
	@Column(nullable = true)
	private Long population;
	
	@Column(nullable = true , length = 600)
	private String description;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.PERSIST },
			fetch = FetchType.LAZY)
	@JoinColumn(name = "state")
	@JsonIgnore
	private State state;
	
	public District() {
		
	}


	public District(String districtName) {
		super();
		this.districtName = districtName;
	}


	public State getState() {
		return state;
	}


	public void setState(State state) {
		this.state = state;
	}


	public long getDistrictId() {
		return districtId;
	}





	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}





	public String getDistrictName() {
		return districtName;
	}





	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}





	public Long getPopulation() {
		return population;
	}





	public void setPopulation(Long population) {
		this.population = population;
	}





	public String getDescription() {
		return description;
	}





	public void setDescription(String description) {
		this.description = description;
	}
	
}
