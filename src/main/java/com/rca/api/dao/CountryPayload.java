package com.rca.api.dao;

import java.util.List;

import com.rca.api.entities.Country;

public class CountryPayload {

	private List<AppCountry> countries;
	
	
	public CountryPayload() {
		
	}
	
	public List<AppCountry> getCountries() {
		return countries;
	}
	public void setCountries(List<AppCountry> countries) {
		this.countries = countries;
	}
	
	
	
	
	
	
}
