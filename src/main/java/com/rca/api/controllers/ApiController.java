package com.rca.api.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rca.api.dao.AppCountry;
import com.rca.api.dao.AppDistrict;
import com.rca.api.dao.AppState;
import com.rca.api.dao.CountryPayload;
import com.rca.api.entities.ApiRequest;
import com.rca.api.entities.Country;
import com.rca.api.entities.District;
import com.rca.api.entities.State;
import com.rca.api.entities.User;
import com.rca.api.repositories.ApiRequestRepository;
import com.rca.api.repositories.CountryRepository;
import com.rca.api.repositories.DistrictRepository;
import com.rca.api.repositories.StateRepository;
import com.rca.api.repositories.UserRepository;
import com.rca.api.services.CountryService;
import com.rca.api.services.DistrictService;
import com.rca.api.services.NavigationServiceLocal;
import com.rca.api.services.StateService;
import com.rca.api.util.exceptions.ApiKeyException;
import com.rca.api.util.exceptions.GeneralException;

@RestController
@RequestMapping("/api/v1")
public class ApiController {

	@Autowired
	CountryService ctrService;
	
	@Autowired
	CountryRepository ctrRepo;
	
	@Autowired
	StateService stService;
	
	@Autowired
	StateRepository stRepo;
	
	@Autowired
	DistrictService distService;
	
	@Autowired
	DistrictRepository distRepo;
	
    @Autowired
    NavigationServiceLocal navDto;
    
    @Autowired
    CountryRepository appCountryRepo;
    
   @Autowired
   UserRepository userRepo;
   
   @Autowired
   ApiRequestRepository apiRepo;
   
   private  final String API_URL="api/v1/";
	
	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public CountryPayload index(ApiRequest request, @RequestParam  String api_key) throws GeneralException {
		User user = userRepo.findByApiKey(api_key);
		if(user == null) throw new ApiKeyException();
			
		
		request.setApiKey(api_key);
		request.setEndPoint(API_URL);
		request.setStatus(true);
		request.setUser(user);
		apiRepo.save(request);
		CountryPayload countries = new CountryPayload();
		List<AppCountry> appCountries = ctrService.getAllCountries();
        countries.setCountries(appCountries);
		return countries;
	}
	
	
	
	@GetMapping("countries")
	@ResponseStatus(HttpStatus.OK)
	public CountryPayload countries(ApiRequest request,@RequestParam  String api_key) throws GeneralException {
		User user = userRepo.findByApiKey(api_key);
		if(user == null) throw new ApiKeyException();
		request.setApiKey(api_key);
		request.setEndPoint(API_URL+"countries");
		request.setStatus(true);
		request.setUser(user);
		apiRepo.save(request);
		CountryPayload countries = new CountryPayload();
		List<AppCountry> appCountries = ctrService.getAllCountries();
        countries.setCountries(appCountries);
		return countries;
	}
	
	
	@GetMapping("countries/{countryName}")
	@ResponseStatus(HttpStatus.OK)
	public Map<String,AppCountry> countryByName(ApiRequest request,@PathVariable String countryName , @RequestParam  String api_key) {
		User user = userRepo.findByApiKey(api_key);
		if(user == null) throw new ApiKeyException();
		request.setApiKey(api_key);
		request.setEndPoint(API_URL+"countries/"+countryName);
		request.setStatus(true);
		request.setUser(user);
		apiRepo.save(request);
		AppCountry country = ctrService.getCountry(countryName);

		
		return Collections.singletonMap("country",country);
	}
	
	@GetMapping("countries/{countryName}/states")
	@ResponseStatus(HttpStatus.OK)
	public Map<String,List<AppState>> states(ApiRequest request,@PathVariable String countryName ,@RequestParam  String api_key) {
		User user = userRepo.findByApiKey(api_key);
		if(user == null) throw new ApiKeyException();
		request.setApiKey(api_key);
		request.setEndPoint(API_URL+"countries/"+countryName+"/states");
		request.setStatus(true);
		request.setUser(user);
		apiRepo.save(request);
		Country country = ctrRepo.findByNameIgnoreCase(countryName);
		Long countryId = new Long(0);
		if(country != null) {
			countryId = country.getId();
		}

		List<AppState> state = stService.getStates(countryId);
		
		return Collections.singletonMap("states",state);
	}
	
	@GetMapping("countries/{countryName}/states/{stateName}")
	@ResponseStatus(HttpStatus.OK)
	public Map<String,AppState> statesByName(@PathVariable String countryName, @PathVariable String stateName ,@RequestParam  String api_key) {
		
		Country country = ctrRepo.findByNameIgnoreCase(countryName);
		Long countryId = new Long(0);
		if(country != null) {
			countryId = country.getId();
		}
		
		
	
		//Get country Id
		AppState state = stService.getStateByName(countryId,stateName);
		
		return Collections.singletonMap("states",state);
	}
	
	
	
	@GetMapping("countries/{countryName}/states/{stateName}/districts")
	@ResponseStatus(HttpStatus.OK)
	public Map<String,List<AppDistrict>> getDistricts(@PathVariable String countryName, @PathVariable String stateName ,@RequestParam  String api_key) {
		
		Country country = ctrRepo.findByNameIgnoreCase(countryName);
		Long countryId = new Long(0);
		if(country != null) {
			countryId = country.getId();
		}
		
	
		//Get country Id
		State state = stRepo.findByNameIgnoreCase(countryId, stateName);
		
		Long stateId = new Long(0);
		
		if(state != null ) {
		   stateId = state.getId();	
		}
		List<AppDistrict> district = distService.getDistricts(stateId);
		
		return Collections.singletonMap("districts",district);
	}
	
    @GetMapping("countries/{countryName}/states/{stateName}/districts/{districtName}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,AppDistrict> getDistrict(@PathVariable String countryName, @PathVariable String stateName,@PathVariable String districtName, @RequestParam String api_key){
    	
    	Country country = ctrRepo.findByNameIgnoreCase(countryName);
		Long countryId = new Long(0);
		if(country != null) {
			countryId = country.getId();
		}
		
	
		//Get country Id
		State state = stRepo.findByNameIgnoreCase(countryId, stateName);
		
		Long stateId = new Long(0);
		
		if(state != null ) {
		   stateId = state.getId();	
		}
		
		
		District district = distRepo.getDistrict(stateId,districtName);
		AppDistrict appDistrict = distService.getAppDistrict(district);
		
		return Collections.singletonMap("district",appDistrict);
    	
    }
	
	
	
	
}
